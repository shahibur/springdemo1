#include<bits/stdc++.h>
#define pb push_back
#define repeat(n) for(int i = 0;i < n;i++)
#define ll long long
#define FastIo ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
using namespace std;

const int mod = 1e9+7;

void show(auto v){
    for(auto t: v)cout << t << " ";
    cout <<endl;
}
vector<ll> lcm_list;

void solve(){

	ll n;
       	cin >> n;
       	ll ans = 0;
	ll rem = n;
	ll added;
	ll prev = 1;
	for(ll i = 0;i < lcm_list.size();i++){
		
		added =  prev % (i+2) == 0? 0 : rem-(n/lcm_list[i]);
		ans = (ans + added*(i+2)) % mod;
		rem = rem - added;
		//cout << "i = "<<i+2<<" added = " <<added<< " rem = " << rem<<endl;
		if(rem <= 0)
			break;
		prev = lcm_list[i];
	}
	cout << ans <<endl;

		



}
int main(){
	FastIo;
	int T = 1;
	//pre-computation
	ll factor = 1;
	for(ll i = 2;i <= 41; i++){
		if(factor%i!=0)
			factor = (factor * i) / __gcd(i,factor);
		lcm_list.pb(factor);
	}
	
       	cin >> T; // comment this line if there is no test case 
       	while(T--){
		solve();
	}
}

